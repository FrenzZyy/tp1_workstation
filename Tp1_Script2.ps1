﻿#Script qui execute des action entré en argument 
#Auteur : Desmaison Martial
#Date : 22/10/2020

if ($args[0] -match "lock") {
    Start-Sleep -Seconds $args[1]                #Le if permet de verouillez l'ecran au bout d'un temps entré en argument
    rundll32.exe user32.dll, LockWorkStation
}
elseif ($args -match "shutdown") {
    shutdown /s /t $args[1]                      #Le eslseif sert a eteindre l'ordinateur au bout d'un temps entré en argument
}
else {
    echo "wrong arguments"                       #Le else permet de dire que l'argument entré est bon ou non
}