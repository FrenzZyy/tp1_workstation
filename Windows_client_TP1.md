# Windows client TP1

# I.Self-footprinting

## Host OS 

### Déterminer les principales informations de votre machine 

* Nom de la machine, OS et version :
```
C:\Users\marti> systeminfo
Nom de l’hôte:                              PC-MARTIAL
Nom du système d’exploitation:              Microsoft Windows 10 Famille 
Version du système:                         10.0.19041 N/A version 19041
[...]
```
* Architecture processeur
```
C:\Users\marti>set
[...]~~
PROCESSOR_ARCHITECTURE=AMD64
[...]
```
* Quantité de RAM
```
C:\Users\marti> systeminfo
[...]
Mémoire physique totale:                    8 068 Mo
[...]
```
* Modèle de la RAM
```
C:\Users\marti>wmic MemoryChip
      Manufacturer  PartNumber 
[...] Samsung       Physical Memory M471A5244CB0-CTD            
      Kingston      Physical Memory ACR26D4S9S1KA-4 [...]   
```
## Devices : 

### Trouver 

* La Marque et le Modèle du processeur
```
C:\Users\marti>wmic cpu get caption, deviceid, name, numberofcores, maxclockspeed, status
Caption                               DeviceID  MaxClockSpeed 
Intel64 Family 6 Model 78 Stepping 3  CPU0      2496      
Name                                      NumberOfCores  Status
Intel(R) Core(TM) i5-6300U CPU @ 2.40GHz  2              OK
```
1. le(s) Nombre de processeur(s) et le(s) nombre de coeur(s)
```
C:\Users\marti>wmic cpu get NumberOfCores, NumberOfLogicalProcessors/Format:List

NumberOfCores=2
NumberOfLogicalProcessors=4
```
2. Si c'est un proc Intel, expliquer le nom du processeur

Dans mon cas c'est un processeur i5 de 6e génération, le nombre 300 symbolise sa place dans la famille et le U signifie underpowered(basse consommation)


* La Marque et le Modèle
1. Du touchpad/trackpad
```
C:\WINDOWS\system32> gwmi Win32_PointingDevice                                                                    

__GENUS                     : 2
__CLASS                     : Win32_PointingDevice
__SUPERCLASS                : CIM_PointingDevice
__DYNASTY                   : CIM_ManagedSystemElement
__PROPERTY_COUNT            : 33
__DERIVATION                : {CIM_PointingDevice, CIM_UserDevice, CIM_LogicalDevice, CIM_LogicalElement...}
__SERVER                    : PC-MARTIAL
__NAMESPACE                 : root\cimv2
__PATH                      : \\PC-MARTIAL\root\cimv2:Win32_PointingDevice.DeviceID="HID\\SYNA7DB5&COL01\\5&22D87139
                              &1&0000"
Availability                :
Caption                     : Souris HID
ConfigManagerErrorCode      : 0
ConfigManagerUserConfig     : False
CreationClassName           : Win32_PointingDevice
Description                 : Souris HID
DeviceID                    : HID\SYNA7DB5&COL01\5&22D87139&1&0000
DeviceInterface             : 162
DoubleSpeedThreshold        :
ErrorCleared                :
ErrorDescription            :
Handedness                  :
HardwareType                : Souris HID
InfFileName                 : msmouse.inf
InfSection                  : HID_Mouse_Inst.NT
InstallDate                 :
IsLocked                    :
LastErrorCode               :
Manufacturer                : Microsoft
Name                        : Souris HID
NumberOfButtons             : 0
PNPDeviceID                 : HID\SYNA7DB5&COL01\5&22D87139&1&0000
PointingType                : 2
PowerManagementCapabilities :
PowerManagementSupported    : False
QuadSpeedThreshold          :
Resolution                  :
SampleRate                  :
Status                      : OK
StatusInfo                  :
Synch                       :
SystemCreationClassName     : Win32_ComputerSystem
SystemName                  : PC-MARTIAL
PSComputerName              : PC-MARTIAL

```
2. De la carte graphique
```
C:\Users\marti>wmic path win32_VideoController get name

Name
Intel(R) HD Graphics 520
```
### Disque dur

* identifier la marque et le modèle de votre(vos) disque(s) dur(s)
```
C:\Users\marti>diskpart
DISKPART> list disk

  N° disque  Statut         Taille   Libre    Dyn  GPT
  ---------  -------------  -------  -------  ---  ---
  Disque 0    En ligne        476 G octets      0 octets
  
DISKPART> select disk 0
Le disque 0 est maintenant le disque sélectionné.

DISKPART> detail disk
KINGSTON RBUSNS8154P3512GJ1
{...}
```
* identifier les différentes partitions de votre/vos disque(s) dur(s)
```
DISKPART> select disk 0
DISKPART> list partition

  N° partition   Type              Taille   Décalage
  -------------  ----------------  -------  --------
  Partition 1    Système            100 M   1024 K
  Partition 2    Réservé             16 M    101 M
  Partition 3    Principale         475 G    117 M
  Partition 4    Récupération      1024 M    475 G
```
* déterminer le système de fichier de chaque partition
```
DISKPART> select partition 1

La partition 1 est maintenant la partition sélectionnée.

DISKPART> detail partition

Partition 1
Type    : c12a7328-f81f-11d2-ba4b-00a0c93ec93b
Masqué  : Oui
Requis  : Non
Attrib  : 0X8000000000000000
Décalage en octets : 

//Le systeme de fichier de chaque partition ce trouve dans la colone
Fs (ex : partition 1 Fs = FAT32)

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 2         ESP          FAT32  Partition    100 M   Sain       Système

DISKPART> select partition 2

La partition 2 est maintenant la partition sélectionnée.

DISKPART> detail partition

Partition 2
Type    : e3c9e316-0b5c-4db8-817d-f92df00215ae
Masqué  : Oui
Requis  : Non
Attrib  : 0X8000000000000000
Décalage en octets : 105906176

Il n’y a pas de volume associé avec cette partition.

DISKPART> select partition 3

La partition 3 est maintenant la partition sélectionnée.

DISKPART> detail partition

Partition 3
Type    : ebd0a0a2-b9e5-4433-87c0-68b6b72699c7
Masqué  : Non
Requis  : Non
Attrib  : 0000000000000000
Décalage en octets : 122683392

//Le systeme de fichier de chaque partition ce trouve dans la colone
Fs 
  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 0     C   Acer         NTFS   Partition    475 G   Sain       Démarrag

DISKPART> select partition 4

La partition 4 est maintenant la partition sélectionnée.

DISKPART> detail partition

Partition 4
Type    : de94bba4-06d1-4d40-a16a-bfd50179d6ac
Masqué  : Non
Requis  : Oui
Attrib  : 0X8000000000000001
Décalage en octets : 511036096512

//Le systeme de fichier de chaque partition ce trouve dans la colone
Fs 
  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 3         Recovery     NTFS   Partition   1024 M   Sain       Masqué
```
* Expliquer la fonction de chaque partition
```
C:\Users\marti> Get-Partition

   DiskPath : \\?\scsi#disk&ven_nvme&prod_kingston_rbusns8#4&2bc9fa1d&0&030000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

PartitionNumber  DriveLetter Offset                                        Size Type
---------------  ----------- ------                                        ---- ----
1                           1048576                                     100 MB System
2                           105906176                                    16 MB Reserved
3                C           122683392                                475.83 GB Basic
4                           511036096512                                  1 GB Recovery
```
**fonction des partition** : 

System est là ou est l’os du pc
Reserved est réservé à la mémoire ram
Basic est le lieu de stockage des données utilisateur
Recover est une partition de secours au cas où le fonctionnement du pc est corrompu

## Users

### Déterminer la liste des utilisateurs de la machine

* la liste complète des utilisateurs de la machine
```
C:\Users\marti>net user

comptes d’utilisateurs de \\PC-MARTIAL

-------------------------------------------------------------------
Administrateur           DefaultAccount        Invité
marti                    sophi                 WDAGUtilityAccount
La commande s’est terminée correctement.
```
* déterminer le nom de l'utilisateur qui est full admin sur la machine
```
C:\Users\marti> $objSID = New-Object System.Security.Principal.SecurityIdentifier("S-1-5-20") 
$objAccount = $objSID.Translate([System.Security.Principal.NTAccount]) 
$objAccount.Value

AUTORITE NT\SERVICE RÉSEAU
```
## Processus

###  Déterminer la liste des processus de la machine
```
C:\WINDOWS\system32> get-process

Handles  NPM(K)    PM(K)      WS(K)     CPU(s)     Id  SI ProcessName
-------  ------    -----      -----     ------     --  -- -----------
    111       8     2172       5368       0,02   4424   0 ACCSvc
    613      38    37632      42260       0,94   5544   1 AcerRegistrationBackGroundTask
    157       9     2052       6756       0,97   4404   0 AdminService
    658      33    19664      36152      11,44  11544   0 afwServ
    558      32    22232      39956       1,27   6944   1 ApplicationFrameHost
    159       9     1964       7392       0,05   3832   1 AppVShNotify
    153       8     1664       6724       0,03   5460   0 AppVShNotify
   1115      30    32092      48240      24,45  13972   0 aswidsagent
   1019      40    40588      66132      78,31   6392   0 aswToolsSvc
    171      12     7364      12300      20,30  16620   0 audiodg
   6139     108   157708     248696     138,67   6216   0 AvastSvc
    580      45   114336     169428      80,13   1652   1 AvastUI
    671      31    16796      45768       5,09   1976   1 AvastUI
   2356      59    42908     112664      64,31   7096   1 AvastUI
    647      36    21976      57460      41,38  13128   1 AvastUI
    526      29    11768      35176       0,95  13708   1 AvastUI
    543      27    22096       1944       0,80   2248   1 Calculator
    372      27   113596     147068     278,19     32   1 chrome
    256      17    40892      69984       4,63    240   1 chrome
    486      32   113324      70264      21,86    260   1 chrome
    301      20    69804      53608       6,38   1404   1 chrome
    234      18    54472      49824       3,36   1836   1 chrome
    341      23    52616      51144       4,42   2016   1 chrome
    220      15    18048      32888       0,41   2804   1 chrome
    221      16    29352      40572       5,06   2836   1 chrome
    329      22    81252     112212      36,27   2920   1 chrome
    215      14    16776      30100       0,30   3536   1 chrome
    242      21    38488      53772       4,50   3636   1 chrome
   3645     104   185648     228492     228,09   3920   1 chrome
    217      15    27200      35848       0,64   4028   1 chrome
    181      13     9692      18444       1,69   4032   1 chrome
    296      21    39592      40000       0,58   4548   1 chrome
    304      20    57932      90828       9,52   4980   1 chrome
    283      22    40724      72748       1,52   5240   1 chrome
    315      22    66616      88112       5,00   5568   1 chrome
    254      17    43028      68060       4,23   6056   1 chrome
    244      16    39392      57116       3,53   6352   1 chrome
    260      17    44892      66872       3,80   6364   1 chrome
    222      16    26252      34732       0,58   6580   1 chrome
    295      21    40144      39732       0,75   6676   1 chrome
    254      17    42720      66784       4,06   6688   1 chrome
    544      32    32600      49304      55,41   6884   1 chrome
    292      24    68400      66392       5,20   7536   1 chrome
    409      24    93972      65320      19,63   7756   1 chrome
   1304      52   373812     315772     184,44   8756   1 chrome
    291      18     7368      17432      26,81   8788   1 chrome
    226      16    96240     114036      23,41   9592   1 chrome
    257      19    32876      48628       1,06   9856   1 chrome
    305      16    28516      42496       1,27   9920   1 chrome
    327      22    56700      55284       3,09   9952   1 chrome
    250      18    43908      67960       3,67   9976   1 chrome
    220      16    25584      33464       0,59  10132   1 chrome
    358      17    37116      60616       4,39  10164   1 chrome
    316      22    72072      90068       8,14  10692   1 chrome
    369      36   243848     281724     108,75  10924   1 chrome
    221      16    25112      32696       0,45  12184   1 chrome
    499       9     1804       6724       0,08  12500   1 chrome
    270      18    36376      61012       4,77  12532   1 chrome
    235      18    30792      34576       0,52  12632   1 chrome
    295      21    39756      40004       0,69  12704   1 chrome
    379      17    37736      37928       1,52  12784   1 chrome
    290      23    52192      89220       6,23  13100   1 chrome
    291      21    32740      49940       1,25  13300   1 chrome
    296      21    32796      55408       2,73  13864   1 chrome
    234      18    18712      35488       0,42  15708   1 chrome
    240      19    26468      50028       0,83  15732   1 chrome
    299      21    43828      76984       4,34  16288   1 chrome
    291      21    33356      44600       1,86  16392   1 chrome
    299      21    45136      66464       4,81  16440   1 chrome
    291      21    31672      61104       0,97  16692   1 chrome
    291      21    31712      44972       0,86  16752   1 chrome
    232      18    26704      34704       0,89  16832   1 chrome
    282      23    69328     111800      14,36  17244   1 chrome
    935      44    23880       2048       1,45  12168   1 commsapps
    265      13     4720      14600       5,33   2912   1 conhost
    764      54    32204      65488       1,25  10816   1 Cortana
    660      27     1792       4536       3,03    712   0 csrss
    698      30     2500       5500      26,30    808   1 csrss
    510      19    24532      38744      11,95   7476   1 ctfmon
    419      19     5912      16016       1,23   2380   0 dasHost
    138       9     2440       4944       0,02   4440   0 DbxSvc
    959      57    42152      80560     227,28  10584   1 Discord
    740      28    77840      75228      44,19  11060   1 Discord
    458      24    13308      36988       8,06  11120   1 Discord
    238      18     8856      49192       0,11  11564   1 Discord
    887     102   211044     175276   1 679,92  11680   1 Discord
    315      21     9984      54908       1,16  11972   1 Discord
    200      16     3096      10556       0,11   7128   0 dllhost
    255      23     6400      15212       0,55   8048   1 dllhost
    122       7     1400       7188       0,30  12000   1 dllhost
    219      14     2084       2500       0,27   7540   0 DropboxUpdate
   1322      55    87444      88612     874,84   1304   1 dwm
    366      25    58032       4332       0,88   9264   1 ePowerButton_NB
   4115      92    76896     132704      65,20   1796   1 explorer
     39       8     3764       7296       0,64    524   1 fontdrvhost
     39       6     1524       3124       0,09    676   0 fontdrvhost
    169      10     1620       1120       0,08   3624   0 GoogleCrashHandler
    152       9     1656       1080       0,05   9788   0 GoogleCrashHandler64
    280      15     4628       2280       0,67   2760   1 HostAppServiceUpdater
    573      29    17536      54396       1,38   9156   1 HxAccounts
    621      26     8308      28964       1,56  10600   1 HxTsr
    420      31    54680      45304       3,06   6068   0 IAStorDataMgrSvc
    306      21    34288      31548       0,44  12428   1 IAStorIcon
      0       0       60          8                 0   0 Idle
    169      10     1764       8148       0,06   2748   0 igfxCUIService
    696      24     7032      24956       5,61   1068   1 igfxEM
    156      11     2896       9440       0,08   3960   1 igfxext
    383      24    25496      19984       1,09   4604   0 IntelAudioService
    163       8     1488       7164       0,08   4416   0 IntelCpHDCPSvc
    133       7     1372       6628       0,02   5884   0 IntelCpHeciSvc
    135       8     1292       5656       0,03   5724   0 jhi_service
    530      56    36288      41844       3,97   5088   0 Lavasoft.WCAssistant.WinService
    266      15     3096       9944       0,36   4684   0 LMS
     43       6     1076       2656       0,13    892   0 LsaIso
   1809      33     9612      24764     138,64    900   0 lsass
      0       0     1100     441192      64,45   2620   0 Memory Compression
    951      50    51992       1992      12,31   1664   1 Microsoft.Photos
   2888      78   178724      12816     136,77   4696   0 NortonSecurity
   1253      51    22548      10560      22,63   8148   1 NortonSecurity
    185      10     2300       8344       0,47   4884   0 nsWscSvc
    736      29    49108      39448      33,19   8640   0 OfficeClickToRun
    665      31    64804      68860       2,11   9232   1 powershell
    237      28    27452      18048       0,42   5044   0 PresentationFontCache
    235      14     9768      14328      14,23   3204   1 QAAdminAgent
    158      11     1872       9576       0,13  11404   1 QAAgent
    175      11     1924       9824       0,14   5032   1 QALockHandler
    235      11     3024      10164       0,13   1560   0 QASvc
    110       7     1216       5020       0,08   4740   0 QcomWlanSrvx64
      0      15    12608      93420       2,56    108   0 Registry
    138       8     1412       5132       0,02   4848   0 RstMwService
    306      13     2484       8628       0,59   4872   0 RtkAudUService64
    464      14     4008      12168       5,91  10452   1 RtkAudUService64
    467      21     6832      28584       2,25     60   1 RuntimeBroker
    346      17     4028      24900       0,83   7456   1 RuntimeBroker
    368      20     6968      27380       3,45   8708   1 RuntimeBroker
    697      29    13356      41036       7,66   9120   1 RuntimeBroker
    442      20    11264      29172      13,11   9452   1 RuntimeBroker
    405      18     4236      19516       0,73   9820   1 RuntimeBroker
    121       8     1484       7304       0,08   9924   1 RuntimeBroker
    303      16     4172      19452      15,08  10032   1 RuntimeBroker
    476      21     6184      28472       1,27  10960   1 RuntimeBroker
   1743     146   225100     104524      38,89   8848   1 SearchApp
    761      74    38632      47204      36,55   6340   0 SearchIndexer
      0       0      184      22740       0,00     56   0 Secure System
    412      16     5328      13916      86,16  10360   0 SecurityHealthService
    163       9     1948       9140       0,47  10324   1 SecurityHealthSystray
    844      11     6096       9328      16,94    868   0 services
    608      26     9700       8388      18,78   9764   1 SettingSyncHost
    115       7     6428       7532       5,03  13240   0 SgrmBroker
    967      38    35932      69024       8,50   9392   1 ShellExperienceHost
    638      18     7564      27956      10,06   8180   1 sihost
     53       3     1068       1100       0,19    628   0 smss
    462      20     4960      18904       0,64  11312   1 SpeechRuntime
    472      24     6008      15732       0,56   4196   0 spoolsv
    138       9     1864       7452       0,08   4952   0 sqlwriter
    726      33    39828      75808       4,36   8600   1 StartMenuExperienceHost
     54       4      800       3220       0,00    716   0 svchost
    465      19    11004      27660       7,17    888   1 svchost
   1327      25    14076      34068      30,83   1008   0 svchost
    117       8     1416       6292       0,05   1164   0 svchost
   1525      20     9632      18584      41,95   1172   0 svchost
    323      13     2784       8036       4,16   1224   0 svchost
     98       8     1132       4956       0,02   1396   0 svchost
    106       7     1232       4808       0,02   1456   0 svchost
    153      10     1800       7744       0,23   1464   0 svchost
    150       9     1884      12148       0,58   1480   0 svchost
    229      11     2340      10336       0,59   1492   0 svchost
    247      13     2492       9840       0,42   1512   0 svchost
    178       7     1544       5708       0,83   1580   0 svchost
    146       9     2080      10880       0,52   1636   0 svchost
    104       7     1248       5656       0,14   1648   0 svchost
    410      10     2208       9448       0,11   1692   0 svchost
    420      17     6696      14828      17,44   1700   0 svchost
    295      16     6304      16984       0,86   1720   0 svchost
    134      10     1576       5920       0,03   1828   0 svchost
    437      13    14976      14844       2,50   1872   0 svchost
    238      14    11704      12516       3,58   1916   0 svchost
    143       9     1648       8640       0,14   1956   0 svchost
    135       9     1520       6436       0,06   2044   0 svchost
    316      10     2836      10452       3,05   2072   0 svchost
    153      33     6376      10092       2,39   2084   0 svchost
    249      11     2700       7440      10,58   2176   0 svchost
    194       9     1932       7424       0,20   2208   0 svchost
    515      23     8912      43564       7,50   2268   1 svchost
    186      10     7816      17656      26,83   2328   0 svchost
    203      12     2728      11436       3,66   2360   0 svchost
    175       9     1972       7220       0,39   2400   0 svchost
    229      12     2564      12860      76,44   2452   0 svchost
    239       7     1236       5436       0,16   2480   0 svchost
    161       9     1724       7876       0,02   2588   0 svchost
    402      16     5468      12860      15,22   2612   0 svchost
    222      15     2164       8572       0,17   2640   0 svchost
    230      10     2132       7480       2,03   2808   0 svchost
    152       9     1616       7088       0,34   2820   0 svchost
    449      12     3708       9512       9,78   2860   0 svchost
    227      12     2424      10600       0,70   2980   0 svchost
    178       9     1844       7004       5,11   3008   0 svchost
    337      12     3492      10660       2,45   3052   0 svchost
    201      13     2528      11540       0,20   3064   0 svchost
    538      23     7028      20532       2,13   3092   0 svchost
    318      15     4020       9184      65,94   3136   0 svchost
    260      15     2792       9144       2,69   3152   0 svchost
    300      20     3024       8680       2,59   3168   0 svchost
    129       8     1444       6392       0,08   3180   0 svchost
    224       8     1628       7336       0,16   3316   0 svchost
    474      16     8252      18980       8,05   3388   0 svchost
    447      33    11240      20144      40,39   3408   0 svchost
    222      11     2376       7928       0,14   3544   0 svchost
    174      12     2480       9564       0,33   3712   0 svchost
    126       8     1416       5968       0,05   3808   0 svchost
    505      13     4032      13808       4,84   3840   0 svchost
    137      11     1708       6164       0,66   3936   0 svchost
    376      15     2780      10800       0,92   3944   0 svchost
    176      10     1936       7444       0,53   4280   0 svchost
    412      29     6420      18000      96,41   4432   0 svchost
    541      26    15916      27192       7,23   4460   0 svchost
    357      22    43220      39136      42,98   4524   0 svchost
    258      13     2592       7396       0,98   4580   0 svchost
    580      23     6392      17560       9,50   4636   0 svchost
    202      11     2276       8512       0,44   4716   0 svchost
    127       9     1576       5916       0,11   4840   0 svchost
    122       7     1244       5112       0,00   5004   0 svchost
    411      20     4812      21040       1,45   5132   0 svchost
    459      24     5072      18488       0,91   5228   1 svchost
    392      24     3356      11640       0,58   5560   0 svchost
    100       7     1240       4928       0,08   5588   0 svchost
    357      19     5004      20936       0,63   6088   0 svchost
    197      12     2332      10528       0,23   6900   0 svchost
    428      21     5388      20772       2,03   7236   0 svchost
    306      13     3636      16468       5,06   7452   0 svchost
    423      20     7536      31960       3,92   8264   1 svchost
    286      13     2644      12076       0,20   8328   0 svchost
    211      11     2696      11284      16,88  10116   0 svchost
    235      13     3016      10484       1,13  10136   0 svchost
    180      10     1992       9500       4,59  10608   0 svchost
    342      13     4908      12768       1,22  10684   0 svchost
    115       7     1508       5892       0,39  10772   0 svchost
    265      15     3692      19452       0,92  11024   0 svchost
    186      14     6144       9416       0,09  11304   0 svchost
    231      13     2816      13520       0,27  11352   0 svchost
    338      19     5312      19312       1,08  11604   1 svchost
    322      19     4580      15764      19,33  12572   0 svchost
    182      11     2332       9784       0,09  13256   0 svchost
    135       9     1808       8532       0,06  13316   1 svchost
    154      10     1880       7308       1,17  14052   0 svchost
    178      10     2024       8004       0,16  17320   0 svchost
   7855       0      204       1576     892,50      4   0 System
    790      39    22728       9844       1,13  15100   1 SystemSettings
    493      24     7212      29740       0,97  15416   1 SystemSettingsBroker
    315      36     8156      18376       1,94   7588   1 taskhostw
    535      22    12548      42732       3,00   5264   1 TextInputHost
   1846      38    58860      92964     108,52   6808   0 TuneupSvc
   1112      40    16872      61876      12,73   6640   1 TuneupUI
    515      48    73004     106188      41,56  13820   1 TuneupUI
    608      26     9500      26608       0,22  14176   1 TuneupUI
    119       8     1356       7236       0,22   5168   1 unsecapp
    119       8     1400       7432       0,11   7924   1 unsecapp
    120       7     1308       6804       0,02  14544   0 unsecapp
    113      10     2076       9336       0,08  16488   1 UserOOBEBroker
    549      32    18000       2028       0,50  10996   1 Video.UI
    124      11     2068       5348       0,98   5080   0 vmnat
     81       8     7724       3760       0,22   5060   0 vmnetdhcp
    314      17     6524      10052     139,33   5024   0 vmware-authd
    220      13     2896      10408       0,56   5108   0 vmware-usbarbitrator64
    166      11     1372       5792       0,13    800   0 wininit
    271      13     2940      11976       0,58    952   1 winlogon
    196      13     3432      10320       1,22   6600   0 WmiPrvSE
    309      10     4616      12588       0,56   9712   0 wsc_proxy
    207       8     3508       5816       0,05   1060   0 WUDFHost
    747      47    26032       1540       1,13   9732   1 YourPhone
```
* choisissez 5 services système et expliquer leur utilité

**lsass.exe** 

lsass.exe vérifie les utilisateurs qui s'enregistrent à un ordinateur Windows, traite les modifications de mot de passe et crée des jetons d'accès qui encapsulent les informations essentielles sur la sécurité.

**Taskhostw.exe**

Taskhostw.exe est un processus hôte pour Tâches Windows , c'est un fichier servant d’hôte aux processus basés sur les DLL. Dans le gestionnaire de tâches, ces processus sont affichés avec le nom Processus hôte pour les tâches Windows.

**wininit.exe**

Wininit.exe est responsable de l'exécution du processus d'initialisation  Windows 

**smss.exe**

A partir de là, smss.exe effectue les opérations suivantes :
    Crée des variables d'environnement
    Charge la clé BootExecute ce qui permet de lancer un chkdsk au démarrage de Windows
    Démarre les modes noyau et utilisateur du sous-système Win32. Ce sous-système comprend
        win32k.sys (mode noyau),
        winsrv.dll (mode utilisateur)
        csrss.exe (mode utilisateur).
    Crée des mappages de périphériques DOS(COMX, LPT, CON, etc
    Crée des fichiers de pagination de mémoire virtuelle
    Lance winlogon.exe, le gestionnaire de connexion Windows

**svchost.exe** 

Au démarrage, svchost.exe vérifie le registre pour des services chargeant un fichier .dll externe et les démarrent.

* déterminer les processus lancés par l'utilisateur qui est full admin sur la machine

```
C:\WINDOWS\system32> Get-Process -IncludeUserName

Handles      WS(K)   CPU(s)     Id UserName               ProcessName
-------      -----   ------     -- --------               -----------
    878      12400    39,39  11524 PC-MARTIAL\marti       ACCStd
    111       5364     0,16   3872 AUTORITE NT\Système    ACCSvc
    141       5968     1,89   3908 AUTORITE NT\Système    AdminService
    621      39060     2,20  10468 PC-MARTIAL\marti       ApplicationFrameHost
    172      14688     0,33  11884 AUTORITE NT\SERVICE... audiodg
    196      16048     0,22   2680 PC-MARTIAL\marti       backgroundTaskHost
    252      39664     1,02    632 PC-MARTIAL\marti       chrome
    320      51996     1,27   1576 PC-MARTIAL\marti       chrome
    177      24860     2,98   1880 PC-MARTIAL\marti       chrome
    267      63812    20,95   2236 PC-MARTIAL\marti       chrome
    284      45220     3,23   2796 PC-MARTIAL\marti       chrome
    329     108960    63,05   2860 PC-MARTIAL\marti       chrome
    272      64896    20,00   3208 PC-MARTIAL\marti       chrome
    263      61976    14,23   3212 PC-MARTIAL\marti       chrome
    283      49204     1,25   3572 PC-MARTIAL\marti       chrome
    323      61448    55,06   4164 PC-MARTIAL\marti       chrome
    311      55644     5,47   4204 PC-MARTIAL\marti       chrome
    339      88820   550,42   4248 PC-MARTIAL\marti       chrome
    227      33308     0,42   4368 PC-MARTIAL\marti       chrome
    286      93616    15,59   5480 PC-MARTIAL\marti       chrome
    299      50544     2,63   5512 PC-MARTIAL\marti       chrome
    273      63760     5,95   5752 PC-MARTIAL\marti       chrome
    283      43412     1,34   6120 PC-MARTIAL\marti       chrome
    290      54636     4,63   6492 PC-MARTIAL\marti       chrome
    223      23920     0,33   7044 PC-MARTIAL\marti       chrome
    368     161212    90,08   7348 PC-MARTIAL\marti       chrome
    223      35208     1,31   7844 PC-MARTIAL\marti       chrome
    351      56864     2,08   8336 PC-MARTIAL\marti       chrome
    358     133888   637,38   9304 PC-MARTIAL\marti       chrome
    285      46128     1,28   9476 PC-MARTIAL\marti       chrome
    275      64432     9,38   9524 PC-MARTIAL\marti       chrome
    308      54456     4,83   9976 PC-MARTIAL\marti       chrome
    312      54316     4,17  10172 PC-MARTIAL\marti       chrome
    225      36788     0,61  10368 PC-MARTIAL\marti       chrome
    296      58208   266,38  10436 PC-MARTIAL\marti       chrome
    258      60800     3,08  10472 PC-MARTIAL\marti       chrome
    222      35888     0,91  10532 PC-MARTIAL\marti       chrome
    233     109020    12,91  10808 PC-MARTIAL\marti       chrome
   3634     232836   867,66  10852 PC-MARTIAL\marti       chrome
    471       6828     0,19  10888 PC-MARTIAL\marti       chrome
   1529     247232 1 247,05  11064 PC-MARTIAL\marti       chrome
    398      44776   137,44  11076 PC-MARTIAL\marti       chrome
    298     111128    27,08  11292 PC-MARTIAL\marti       chrome
    286      43252     1,84  12044 PC-MARTIAL\marti       chrome
    312      88100    59,66  12132 PC-MARTIAL\marti       chrome
    283      16968    85,92  12136 PC-MARTIAL\marti       chrome
    277      42232     1,88  12236 PC-MARTIAL\marti       chrome
    281      51380     2,06  12416 PC-MARTIAL\marti       chrome
    289      58800     4,73  12848 PC-MARTIAL\marti       chrome
    272      48492     2,09  13036 PC-MARTIAL\marti       chrome
    263      46108     2,16  13124 PC-MARTIAL\marti       chrome
    349      65100    27,50  13348 PC-MARTIAL\marti       chrome
    358      61320   113,31  13760 PC-MARTIAL\marti       chrome
    223      38828     0,50  14132 PC-MARTIAL\marti       chrome
    262      60656     5,48  14236 PC-MARTIAL\marti       chrome
    466      56824     8,73  14284 PC-MARTIAL\marti       chrome
    321     104256    36,64  15840 PC-MARTIAL\marti       chrome
    231      36968     0,50  16728 PC-MARTIAL\marti       chrome
     83       5264     0,33   2084 PC-MARTIAL\marti       cmd
     72       3924     0,02   3240 PC-MARTIAL\marti       cmd
    938       2056     1,09  11812 PC-MARTIAL\marti       commsapps
    261      16392     0,08   3408 PC-MARTIAL\marti       conhost
    263      13500     0,33   7336 PC-MARTIAL\marti       conhost
    267      24000    63,98   9168 PC-MARTIAL\marti       conhost
    270      15236     5,63  14048 PC-MARTIAL\marti       conhost
   1066      86976   247,11   5340 PC-MARTIAL\marti       Cortana
    832       4532     1,91    696                        csrss
    917       5712    65,38    788                        csrss
    527      22432    46,31   6620 PC-MARTIAL\marti       ctfmon
    138       4984     0,06   4000 AUTORITE NT\Système    DbxSvc
    974      83300   197,19   2600 PC-MARTIAL\marti       Discord
    800     107976   104,61   3544 PC-MARTIAL\marti       Discord
    238      50320     0,14   8956 PC-MARTIAL\marti       Discord
    468      37908    12,33   9812 PC-MARTIAL\marti       Discord
   1581     234716 1 721,34  10132 PC-MARTIAL\marti       Discord
    317      56300     1,94  10332 PC-MARTIAL\marti       Discord
    112       6620     0,05  13352 PC-MARTIAL\marti       diskpart
    136       7848     0,22   2356 PC-MARTIAL\marti       dllhost
    198      10308     0,08   6728 AUTORITE NT\Système    dllhost
    226       8640     0,11   7464 PC-MARTIAL\marti       dllhost
    269      14028     0,52   8708 PC-MARTIAL\marti       dllhost
    247      15168     0,14  16776 PC-MARTIAL\marti       dllhost
    219       3736     0,30   6216 AUTORITE NT\Système    DropboxUpdate
   1483      94796 1 480,63   1292 Window Manager\DWM-1   dwm
    365       6504     1,36  10596 PC-MARTIAL\marti       ePowerButton_NB
   4979     187936   183,00   6836 PC-MARTIAL\marti       explorer
     39       3060     0,03    772 Font Driver Host\UM... fontdrvhost
     39       6520     0,72   1000 Font Driver Host\UM... fontdrvhost
    169       1128     0,06   9852 AUTORITE NT\Système    GoogleCrashHandler
    152       1080     0,03   9936 AUTORITE NT\Système    GoogleCrashHandler64
    282       2240     0,98   8444 PC-MARTIAL\marti       HostAppServiceUpdater
    581      51808     0,63   4216 PC-MARTIAL\marti       HxAccounts
    607      28416     0,63   3076 PC-MARTIAL\marti       HxTsr
    417      37020     3,30  11832 AUTORITE NT\Système    IAStorDataMgrSvc
    308      28188     0,89   8300 PC-MARTIAL\marti       IAStorIcon
      0          8               0                        Idle
    169       7864     0,13   2384 AUTORITE NT\Système    igfxCUIService
    871      24580     8,61   6688 PC-MARTIAL\marti       igfxEM
    156       8588     0,13  10716 AUTORITE NT\Système    igfxext
    394      19388     0,95   4104 AUTORITE NT\Système    IntelAudioService
    163       6728     0,05   3972 AUTORITE NT\Système    IntelCpHDCPSvc
    133       6040     0,05   4948 AUTORITE NT\Système    IntelCpHeciSvc
    135       5572     0,02   5124 AUTORITE NT\Système    jhi_service
    529      37244     4,64   4632 AUTORITE NT\Système    Lavasoft.WCAssistant.WinService
    267       8732     0,47   4192 AUTORITE NT\Système    LMS
    560      48292     1,00  12708 PC-MARTIAL\marti       LockApp
     43       2684     0,19    936 AUTORITE NT\Système    LsaIso
   1873      23564    58,45    944 AUTORITE NT\Système    lsass
      0     181860   140,98   2304                        Memory Compression
    893       1824     3,19  12616 PC-MARTIAL\marti       Microsoft.Photos
   2409     837788 ...95,19   4612                        MsMpEng
    173       9040     2,28   6432                        NisSrv
   2799      36672   106,59   4232 AUTORITE NT\Système    NortonSecurity
   1117      14708    23,14   6968 PC-MARTIAL\marti       NortonSecurity
    185       7936     0,50   4332                        nsWscSvc
    720      31524     2,69   3964 AUTORITE NT\Système    OfficeClickToRun
    660     123980     9,11  12032 PC-MARTIAL\marti       powershell
    676      76748     1,42  14964 PC-MARTIAL\marti       powershell
    235      15404     0,55   7080 AUTORITE NT\SERVICE... PresentationFontCache
    247      12480    36,19   3108 AUTORITE NT\Système    QAAdminAgent
    158       9512     0,67  12008 PC-MARTIAL\marti       QAAgent
    176       9236     0,19  11488 AUTORITE NT\Système    QALockHandler
    244       9372     0,19  11740 AUTORITE NT\Système    QASvc
    110       5080     0,08   4272 AUTORITE NT\Système    QcomWlanSrvx64
      0      94464     1,55    108                        Registry
    138       5372     0,03   4280 AUTORITE NT\Système    RstMwService
    457      11684     9,31    920 PC-MARTIAL\marti       RtkAudUService64
    285       8940     0,25   4304 AUTORITE NT\Système    RtkAudUService64
    141       8268     0,11  13872 PC-MARTIAL\marti       rundll32
    147      12104     0,02   3920 PC-MARTIAL\marti       RuntimeBroker
    121       7192     0,08   4252 PC-MARTIAL\marti       RuntimeBroker
    807      43332     3,70   4608 PC-MARTIAL\marti       RuntimeBroker
    499      29452     1,59   5712 PC-MARTIAL\marti       RuntimeBroker
    397      30372     7,23   7944 PC-MARTIAL\marti       RuntimeBroker
    429      28044     2,58   8356 PC-MARTIAL\marti       RuntimeBroker
    431      29148     6,17   8384 PC-MARTIAL\marti       RuntimeBroker
    251      15008     0,78   9296 PC-MARTIAL\marti       RuntimeBroker
    485      30876     2,11   9468 PC-MARTIAL\marti       RuntimeBroker
    337      23480     0,31   9608 PC-MARTIAL\marti       RuntimeBroker
    465      33020     4,44  10568 PC-MARTIAL\marti       RuntimeBroker
    356      22028     0,31  13616 PC-MARTIAL\marti       RuntimeBroker
    393      12960     5,45   4340 AUTORITE NT\Système    RvControlSvc
   2078     276144    23,73   8172 PC-MARTIAL\marti       SearchApp
    135       7256     0,05   4740 AUTORITE NT\Système    SearchFilterHost
    820      45208    39,31   8180 AUTORITE NT\Système    SearchIndexer
    266       8300     0,06  10064 AUTORITE NT\Système    SearchProtocolHost
      0      22384     0,00     56                        Secure System
    392      12792    89,36   8920                        SecurityHealthService
    161       8564     0,31   8936 PC-MARTIAL\marti       SecurityHealthSystray
    826       8608    11,38    912                        services
    641       8656    27,19   9072 PC-MARTIAL\marti       SettingSyncHost
    115       7440     4,80   8828                        SgrmBroker
    872      58700     7,61   8604 PC-MARTIAL\marti       ShellExperienceHost
    745      28244    15,81   6996 PC-MARTIAL\marti       sihost
    413      22800     0,17  18064 PC-MARTIAL\marti       smartscreen
     53       1116     0,22    612                        smss
    490      20764     0,45   6084 PC-MARTIAL\marti       SpeechRuntime
    433      12484     0,44   3652 AUTORITE NT\Système    spoolsv
    139       6548     0,08   4348 AUTORITE NT\Système    sqlwriter
    794      86732     8,33   7828 PC-MARTIAL\marti       StartMenuExperienceHost
     54       3184     0,00    652 AUTORITE NT\Système    svchost
   1373      34088    34,69    776 AUTORITE NT\Système    svchost
    390      20648     0,89    972 PC-MARTIAL\marti       svchost
   1573      16868    43,39   1152 AUTORITE NT\SERVICE... svchost
    291       7372    11,45   1208 AUTORITE NT\SERVICE... svchost
    334       8004    28,09   1212 AUTORITE NT\Système    svchost
    234      10124     0,52   1312                        svchost
    140       8308     0,03   1360 PC-MARTIAL\marti       svchost
     98       4916     0,03   1372 AUTORITE NT\Système    svchost
    191       9172     0,36   1436 AUTORITE NT\Système    svchost
    253       9436     0,44   1464 AUTORITE NT\Système    svchost
    154      11296     0,59   1472 AUTORITE NT\SERVICE... svchost
    176       5608     0,38   1524 AUTORITE NT\SERVICE... svchost
    426      15088     9,80   1536 AUTORITE NT\Système    svchost
    363       8072     0,13   1600 AUTORITE NT\Système    svchost
    134       5912     0,06   1624 AUTORITE NT\Système    svchost
    247      12776     0,66   1692 AUTORITE NT\Système    svchost
    447      14604     3,33   1768 AUTORITE NT\SERVICE... svchost
    323       9656     3,39   1852 AUTORITE NT\Système    svchost
    117       6272     0,05   1900 AUTORITE NT\SERVICE... svchost
    156       6448     2,05   1952 AUTORITE NT\SERVICE... svchost
    123       7520     0,03   1964                        svchost
    215      15320    24,45   2076 AUTORITE NT\Système    svchost
    208      11044     4,88   2112 AUTORITE NT\Système    svchost
    171       6516     0,20   2128 AUTORITE NT\SERVICE... svchost
    236      12908    94,33   2152 AUTORITE NT\Système    svchost
    263       5420     0,30   2176 AUTORITE NT\Système    svchost
    234       7516     0,08   2228 AUTORITE NT\SERVICE... svchost
    419      12340    10,09   2320 AUTORITE NT\SERVICE... svchost
    222       8576     0,31   2340 AUTORITE NT\Système    svchost
    230      11208     0,22   2364 AUTORITE NT\Système    svchost
    199       7348     0,92   2448 AUTORITE NT\Système    svchost
    154       7156     0,97   2456 AUTORITE NT\SERVICE... svchost
    434       8936     9,13   2484 AUTORITE NT\SERVICE... svchost
    226      10368     0,27   2588 AUTORITE NT\SERVICE... svchost
    158       6456     0,05   2624 AUTORITE NT\Système    svchost
    203       7820     4,53   2684 AUTORITE NT\SERVICE... svchost
    325       9536     2,30   2716 AUTORITE NT\Système    svchost
    330       9060    68,58   2776 AUTORITE NT\SERVICE... svchost
    129       6184     0,05   2808 AUTORITE NT\Système    svchost
    428      18768    14,13   2940 AUTORITE NT\SERVICE... svchost
    554      24688     7,28   2976 AUTORITE NT\Système    svchost
    434      13528     6,59   3028 AUTORITE NT\SERVICE... svchost
    141       6112     0,58   3216 AUTORITE NT\SERVICE... svchost
    376       8836     0,92   3232 AUTORITE NT\SERVICE... svchost
    126       6048     0,03   3284 AUTORITE NT\Système    svchost
    143       7148     0,09   3320 AUTORITE NT\Système    svchost
    559      19072     3,19   3476 AUTORITE NT\Système    svchost
    406      31128    64,91   3504 AUTORITE NT\SERVICE... svchost
    172       8860     0,88   3516 AUTORITE NT\SERVICE... svchost
    236      13408     0,28   3588 AUTORITE NT\Système    svchost
    135       6320     0,06   3688 AUTORITE NT\SERVICE... svchost
    182       7400     0,48   3728 AUTORITE NT\SERVICE... svchost
    281       7296     0,23   3952                        svchost
    398      13300    29,13   4008 AUTORITE NT\SERVICE... svchost
    261       7876     1,28   4076 AUTORITE NT\SERVICE... svchost
    375      20872     9,42   4092 AUTORITE NT\Système    svchost
    258       7008     1,17   4116 AUTORITE NT\Système    svchost
    443      16872     9,19   4140 AUTORITE NT\Système    svchost
    208       8120     0,45   4220 AUTORITE NT\Système    svchost
    127       5812     0,05   4260 AUTORITE NT\SERVICE... svchost
    122       4960     0,09   4400 AUTORITE NT\Système    svchost
    404      20292     1,81   4620 AUTORITE NT\Système    svchost
    625      16016     6,09   4712 AUTORITE NT\Système    svchost
    100       4904     0,20   4848 AUTORITE NT\SERVICE... svchost
    386      11072     0,14   5072 AUTORITE NT\Système    svchost
    257      11948     0,31   6224 AUTORITE NT\Système    svchost
    161       7548     0,06   6344 AUTORITE NT\Système    svchost
    468      27364    15,36   7024 PC-MARTIAL\marti       svchost
    554      36392    13,95   7120 PC-MARTIAL\marti       svchost
    397      16528     1,22   7180 AUTORITE NT\SERVICE... svchost
    432      30632     3,72   7376 PC-MARTIAL\marti       svchost
    284      19080     1,89   8392 AUTORITE NT\SERVICE... svchost
    183       8944     0,08   8556 AUTORITE NT\Système    svchost
    265      11332     2,22   8752 AUTORITE NT\Système    svchost
    176       6036     0,36   9652 AUTORITE NT\SERVICE... svchost
    401      17836     0,20   9896 AUTORITE NT\Système    svchost
    293       8488     1,34  10024 AUTORITE NT\SERVICE... svchost
    212      11040    10,02  10048 AUTORITE NT\Système    svchost
    326      15236    13,61  10412                        svchost
    105       5908     0,11  10420 AUTORITE NT\Système    svchost
    298      10256     0,38  10460 AUTORITE NT\Système    svchost
    458      17188     1,38  11324 PC-MARTIAL\marti       svchost
    297      12196     0,44  11992 AUTORITE NT\Système    svchost
    419      17552     0,97  13328 AUTORITE NT\Système    svchost
    142      10440     0,55  13592 AUTORITE NT\SERVICE... svchost
    106       5296     0,03  16364 AUTORITE NT\SERVICE... svchost
   5459       3404   731,17      4                        System
    736       1456     1,27   6984 PC-MARTIAL\marti       SystemSettings
    515      30148     0,59   3772 PC-MARTIAL\marti       SystemSettingsBroker
    327      17904     1,88   5908 PC-MARTIAL\marti       taskhostw
    670      55944   260,42   6580 PC-MARTIAL\marti       Taskmgr
    534      42692     6,86   9592 PC-MARTIAL\marti       TextInputHost
    154       8900     0,11  11572 AUTORITE NT\Système    unsecapp
    158       9140     0,14  11852 AUTORITE NT\Système    unsecapp
    271      12528     0,14   8524 AUTORITE NT\Système    vds
    719       1904     0,94   7928 PC-MARTIAL\marti       Video.UI
    128       5956    14,50   4592 AUTORITE NT\Système    vmnat
     81       4588     0,11   4600 AUTORITE NT\Système    vmnetdhcp
    596      47244    73,75   2948 PC-MARTIAL\marti       vmplayer
    327       8772    99,19   4700 AUTORITE NT\Système    vmware-authd
    265      13952     0,25   7864 PC-MARTIAL\marti       vmware-unity-helper
    228       9736     0,28   4720 AUTORITE NT\Système    vmware-usbarbitrator64
    563    1106152 1 823,58   1328 PC-MARTIAL\marti       vmware-vmx
    166       5684     0,13    780                        wininit
    275      11868     0,48    872 AUTORITE NT\Système    winlogon
   1248     104364    26,86   2440 PC-MARTIAL\marti       WINWORD
    197       9900     1,16   2972 AUTORITE NT\Système    WmiPrvSE
    207       5716     0,11   1044 AUTORITE NT\SERVICE... WUDFHost
    798      25828     1,58   8620 PC-MARTIAL\marti       YourPhone
```
## Network

### Afficher la liste des cartes réseau de votre machine
```
C:\Users\marti>ipconfig

Carte(s) réseau:                            5 carte(s) réseau installée(s).
                                            [01]: VMware Virtual Ethernet Adapter for VMnet1
                                                  Nom de la connexion : VMware Network Adapter VMnet1
                                                  DHCP activé :         Oui
                                                  Serveur DHCP :        192.168.171.254
                                                  Adresse(s) IP
                                                  [01]: 192.168.171.1
                                                  [02]: fe80::2161:9afc:33df:9112
                                            [02]: Famatech RadminVPN Ethernet Adapter
                                                  Nom de la connexion : Radmin VPN
                                                  DHCP activé :         Non
                                                  Adresse(s) IP
                                                  [01]: 26.155.195.49
                                                  [02]: fe80::e476:26be:1fcd:9b09
                                                  [03]: fdfd::1a9b:c331
                                            [03]: VMware Virtual Ethernet Adapter for VMnet8
                                                  Nom de la connexion : VMware Network Adapter VMnet8
                                                  DHCP activé :         Oui
                                                  Serveur DHCP :        192.168.31.254
                                                  Adresse(s) IP
                                                  [01]: 192.168.31.1
                                                  [02]: fe80::f4f9:2758:f688:a3b3
                                            [04]: Realtek PCIe GbE Family Controller
                                                  Nom de la connexion : Ethernet
                                                  État :                Support déconnecté
                                            [05]: Qualcomm Atheros QCA9377 Wireless Network Adapter
                                                  Nom de la connexion : Wi-Fi
                                                  DHCP activé :         Oui
                                                  Serveur DHCP :        192.168.0.254
                                                  Adresse(s) IP
                                                  [01]: 192.168.0.19
                                                  [02]: fe80::3047:6547:19f1:2654
                                                  [03]: 2a01:e0a:12c:2ee0:e54b:4c9b:c3ab:d278
                                                  [04]: 2a01:e0a:12c:2ee0:3047:6547:19f1:2654
```
* expliquer la fonction de chacune d'entre elles

**[01]: VMware Virtual Ethernet Adapter for VMnet1** : cet carte permet au machine virtuel editer sur VMware de ce connecter a un reseau
**[02]: Famatech RadminVPN Ethernet Adapter** : cet carte permet a RadminVPN de ce connecter a un reseau
**[03]: VMware Virtual Ethernet Adapter for VMnet8** : cet carte sert a la même chose que la carte Num[01]
**[04]: Realtek PCIe GbE Family Controller** : cet carte est utilisé pour se connecter au reseau ethernet
**[05]: Qualcomm Atheros QCA9377 Wireless Network Adapter** : cet carte est utilisé pour ce connecter a un réseau wi-fi

### Lister tous les ports TCP et UDP en utilisation

* déterminer quel programme tourne derrière chacun des ports

```
C:\WINDOWS\system32> netstat -a -b

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            Pc-Martial:0           LISTENING
  RpcSs
 [svchost.exe]
  TCP    0.0.0.0:445            Pc-Martial:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:902            Pc-Martial:0           LISTENING
 [vmware-authd.exe]
  TCP    0.0.0.0:912            Pc-Martial:0           LISTENING
 [vmware-authd.exe]
  TCP    0.0.0.0:5040           Pc-Martial:0           LISTENING
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:5357           Pc-Martial:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:8733           Pc-Martial:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49664          Pc-Martial:0           LISTENING
 [lsass.exe]
  TCP    0.0.0.0:49665          Pc-Martial:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          Pc-Martial:0           LISTENING
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49667          Pc-Martial:0           LISTENING
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:52899          Pc-Martial:0           LISTENING
 [spoolsv.exe]
  TCP    0.0.0.0:53542          Pc-Martial:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    26.155.195.49:139      Pc-Martial:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:6463         Pc-Martial:0           LISTENING
 [Discord.exe]
  TCP    192.168.0.19:139       Pc-Martial:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.0.19:53574     40.67.254.36:https     ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    192.168.0.19:55186     162.159.135.234:https  ESTABLISHED
 [Discord.exe]
  TCP    192.168.0.19:55301     104.26.3.242:https     TIME_WAIT
  TCP    192.168.0.19:55314     20.190.137.73:https    TIME_WAIT
  TCP    192.168.0.19:55326     98:https               TIME_WAIT
  TCP    192.168.0.19:55361     13.107.6.163:https     TIME_WAIT
  TCP    192.168.0.19:55362     128.116.112.14:https   TIME_WAIT
  TCP    192.168.0.19:55363     45:https               TIME_WAIT
  TCP    192.168.0.19:55381     90:https               TIME_WAIT
  TCP    192.168.0.19:55384     217:https              TIME_WAIT
  TCP    192.168.0.19:55394     40.126.9.77:https      TIME_WAIT
  TCP    192.168.0.19:55401     webpooldb50e25:https   TIME_WAIT
  TCP    192.168.0.19:55406     51.138.106.75:https    TIME_WAIT
  TCP    192.168.0.19:55407     40.90.137.120:https    TIME_WAIT
  TCP    192.168.0.19:55408     52.114.158.53:https    TIME_WAIT
  TCP    192.168.0.19:55409     52.114.158.53:https    TIME_WAIT
  TCP    192.168.0.19:55413     90:https               TIME_WAIT
  TCP    192.168.0.19:55420     bingforbusiness:https  ESTABLISHED
 [SearchApp.exe]
  TCP    192.168.0.19:55424     20.190.129.97:https    ESTABLISHED
 [SearchApp.exe]
  TCP    192.168.0.19:55425     20.190.129.97:https    ESTABLISHED
 [SearchApp.exe]
  TCP    192.168.0.19:55426     20.190.129.97:https    ESTABLISHED
 [SearchApp.exe]
  TCP    192.168.0.19:55429     204.79.197.222:https   ESTABLISHED
 [SearchApp.exe]
  TCP    192.168.31.1:139       Pc-Martial:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.171.1:139      Pc-Martial:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:135               Pc-Martial:0           LISTENING
  RpcSs
 [svchost.exe]
  TCP    [::]:445               Pc-Martial:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:5357              Pc-Martial:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:8733              Pc-Martial:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49664             Pc-Martial:0           LISTENING
 [lsass.exe]
  TCP    [::]:49665             Pc-Martial:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49666             Pc-Martial:0           LISTENING
  EventLog
 [svchost.exe]
  TCP    [::]:49667             Pc-Martial:0           LISTENING
  Schedule
 [svchost.exe]
  TCP    [::]:52899             Pc-Martial:0           LISTENING
 [spoolsv.exe]
  TCP    [::]:53542             Pc-Martial:0           LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:52900            Pc-Martial:0           LISTENING
 [jhi_service.exe]
  TCP    [2a01:e0a:12c:2ee0:dc5c:d025:51d1:4896]:55404  [2a01:111:f100:2000::a83e:3185]:https  ESTABLISHED
 [NortonSecurity.exe]
  TCP    [2a01:e0a:12c:2ee0:dc5c:d025:51d1:4896]:55422  [2603:1026:100:1::2]:https  ESTABLISHED
 [SearchApp.exe]
  TCP    [2a01:e0a:12c:2ee0:dc5c:d025:51d1:4896]:55432  [2a01:111:f100:3000::a83e:191f]:https  ESTABLISHED
 [NortonSecurity.exe]
  UDP    0.0.0.0:500            *:*
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:3544           *:*
  iphlpsvc
 [svchost.exe]
  UDP    0.0.0.0:3702           *:*
  FDResPub
 [svchost.exe]
  UDP    0.0.0.0:3702           *:*
  FDResPub
 [svchost.exe]
  UDP    0.0.0.0:4500           *:*
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:5050           *:*
  CDPSvc
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:5355           *:*
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:63281          *:*
  FDResPub
 [svchost.exe]
  UDP    26.155.195.49:137      *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    26.155.195.49:138      *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    26.155.195.49:1900     *:*
  SSDPSRV
 [svchost.exe]
  UDP    26.155.195.49:60839    *:*
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:1900         *:*
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:53885        *:*
  iphlpsvc
 [svchost.exe]
  UDP    127.0.0.1:60843        *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.0.19:137       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.0.19:138       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.0.19:1900      *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.0.19:52608     *:*
  iphlpsvc
 [svchost.exe]
  UDP    192.168.0.19:60842     *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.31.1:137       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.31.1:138       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.31.1:1900      *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.31.1:60841     *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.171.1:137      *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.171.1:138      *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.171.1:1900     *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.171.1:60840    *:*
  SSDPSRV
 [svchost.exe]
  UDP    [::]:500               *:*
  IKEEXT
 [svchost.exe]
  UDP    [::]:3702              *:*
  FDResPub
 [svchost.exe]
  UDP    [::]:3702              *:*
  FDResPub
 [svchost.exe]
  UDP    [::]:4500              *:*
  IKEEXT
 [svchost.exe]
  UDP    [::]:5353              *:*
  Dnscache
 [svchost.exe]
  UDP    [::]:5355              *:*
  Dnscache
 [svchost.exe]
  UDP    [::]:63282             *:*
  FDResPub
 [svchost.exe]
  UDP    [::1]:1900             *:*
  SSDPSRV
 [svchost.exe]
  UDP    [::1]:60838            *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::2161:9afc:33df:9112%13]:1900  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::2161:9afc:33df:9112%13]:60835  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::3047:6547:19f1:2654%12]:1900  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::3047:6547:19f1:2654%12]:60837  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::e476:26be:1fcd:9b09%24]:1900  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::e476:26be:1fcd:9b09%24]:60834  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::f4f9:2758:f688:a3b3%8]:1900  *:*
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::f4f9:2758:f688:a3b3%8]:60836  *:*
  SSDPSRV
 [svchost.exe]
 ```
 
* expliquer la fonction de chacun de ces programmes

**[svchost.exe]** : Au démarrage, svchost.exe vérifie le registre pour des services chargeant un fichier .dll externe et les démarrent.

**[vmware-authd.exe]** : c'est un pilote Vmware qui agit comme un service d'autorisation et d'authentification pour accéder aux machines virtuelles.

**[lsass.exe]** : lsass.exe vérifie les utilisateurs qui s'enregistrent à un ordinateur Windows, traite les modifications de mot de passe et crée des jetons d'accès qui encapsulent les informations essentielles sur la sécurité.

**[spoolsv.exe]** : spoolsv.exe est le principal service de file d'attente de l'imprimante sous Windows 2000 et les versions suivantes – il s'occupe de gérer tout le travail d'impressions sur l'ordinateur.

**[Discord.exe]** : Discord.exe sert a executer discord

**[searchapp.exe]** : Il s'agit d'un outil de recherche interne géré par Cortana.

**[jhi_service.exe]** : il appartient au logiciel Composants Intel® Management Engine et il Permet aux applications d’accéder au DAL Intel local.

**[NortonSecurity.exe]** : permet de lancer l'antivirus Norton security

# II. Scripting

* Voir Tp1_Script1.Ps1 et Tp1_Script2.Ps1 envoyer a part dans le même dossier que le Tp.

# III. Gestion de softs

### Expliquer l'intérêt de l'utilisation d'un gestionnaire de paquets

Un gestionnaire de paquets est un (ou plusieurs) outil(s) automatisant le processus d'installation, désinstallation, mise à jour de logiciels installés sur un système informatique. Ils permettent de mettre à disposition simplement des milliers de paquetages lors d'une installation standard.Tous les logiciels (les paquets) sont centralisés dans un seul et même serveur (le dépôt) :
* plus besoin de parcourir le Web pour trouver les fichiers d’installation de tel ou tel logiciel, tous se trouvent au même endroit ! On va enfin pouvoir installer et mettre à jour plusieurs logiciels avec une seule commande.
* Tous les logiciels sont dépourvus de spywares et malwares en tout genre : toutes les cases qui installaient des logiciels non désirés n’existent plus.
* Les dépendances sont automatiquement installées : si un logiciel a besoin d’un programme (ex. : Microsoft Visual C++ 2010 Redistributable Package) pour fonctionner, le gestionnaire va l’installer automatiquement.

### Utiliser un gestionnaire de paquet propres à votre OS pour

* lister tous les paquets déjà installés

```
C:\WINDOWS\system32> choco list -l
Chocolatey v0.10.15
chocolatey 0.10.15
1 packages installed.
```

* déterminer la provenance des paquets
```
 C:\WINDOWS\system32> choco source list
Chocolatey v0.10.15
chocolatey - https://chocolatey.org/api/v2/ | Priority 0|Bypass Proxy - False|Self-Service - False|Admin Only - False.
```
# IV. Machine virtuelle

### Partage de fichiers

- Connection ssh
```
C:\WINDOWS\system32> ssh root@192.168.120.51
root@192.168.120.51's password:
Last login: Mon Nov  9 16:33:43 2020 from 192.168.120.1
```
- Sur powershell (partage le dossier)
```
C:\WINDOWS\system32> New-SmbShare -Name Share -Path "C:\Share" -FullAccess "Tout le monde"
```
- Sur la VM 
```
[root@localhost /]# mount -t cifs -o username=martialdesm68@gmail.com,password=[...] //192.168.120.1/Share /opt/partage
[root@localhost /]# cd opt/partage/
[root@localhost partage]# rm file.txt
rm: remove regular empty file ‘file.txt’? y
[root@localhost partage]# ls
test2.txt  test.txt
[root@localhost partage]# touch file.txt
[root@localhost partage]# ls
file.txt  test2.txt  test.txt
```
- A nouveau sur powershell
```
C:\Share> ls


    Répertoire : C:\Share


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----        09/11/2020     18:48              0 file.txt
-a----        09/11/2020     18:16              0 test.txt
-a----        09/11/2020     18:47              0 test2.txt
```
